<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/fonts.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/sweetalert.css" rel="stylesheet">



    <!-- HTML5 Shim and R
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
@include('include.navbar')
<div class="main-container container">
@yield('content')

<!-- Footer -->
<footer>
    <div class="footer myfont">
        <p>Copyright &copy; www.motorbigbike.local 2016</p>
    </div>
</footer>
</div>
    <script src="/assets/js/jquery-3.1.0.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/sweetalert.min.js"></script>
@if (isset($errors) && count($errors) > 0)
    <script>
        $(function(){
            var message = '';
            @foreach($errors->all() as $error)
                    message+='{{$error}}';
            @endforeach
            /*Messagebox.alert(message,null,'<span style="color:red"><i class="fa fa-lock"></i>ล็อคอินเข้าสู่ระบบไม่สำเร็จ</span>');*/
            swal(message);
        });
    </script>
@endif
</body>

</html>
