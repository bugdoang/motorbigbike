<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contain=file_get_contents(dirname(__FILE__).'/province.sql');
        if(!empty($contain))
        {
            DB::table('provinces')->delete();
            DB::statement($contain);
        }
    }
}
