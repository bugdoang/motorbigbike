<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $members = [];
            $members[] = [
                'member_id'=>1,
                'member_name'=>'นาย',
                'first_name'=>'ผู้ดูแลระบบ',
                'last_name'=>'',
                'gender'=>'F',
                'dob'=>'2016-08-11',
                'card_id'=>'123456789',
                'mobile'=>'099999999',
                'email'=>'chompoo@hotmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('1234'),
                'address'=>'xxxx',
                'type_of_member'=>'admin',
                'status_member'=>'verified',
                'avatar'=>'/uploads/default/tiffany.png',
                'province_id'=>1,
            ];

            $members[] = [
                'member_id'=>2,
                'member_name'=>'นางสาว',
                'first_name'=>'ผู้ทดสอบ',
                'last_name'=>'',
                'gender'=>'F',
                'dob'=>'2016-08-11',
                'card_id'=>'123456789',
                'mobile'=>'099999999',
                'email'=>'chompoo@hotmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('1234'),
                'address'=>'xxxx',
                'type_of_member'=>'admin',
                'status_member'=>'verified',
                'avatar'=>'/uploads/default/tiffany.png',
                'province_id'=>1,
            ];

            DB::table('members')->delete();
            DB::table('members')->insert($members);
        }
}
