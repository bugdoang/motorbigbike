<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products',function (Blueprint $table){
            $table->increments('product_id');
            $table->string('product_name',180)->nullable()->comment('ชื่อสินค้า');
            $table->decimal('product_price',8,2)->nullable()->comment('ราคาสินค้า');
            $table->string('product_detail',800)->nullable()->comment('รายละเอียดสินค้า');
            $table->integer('product_view')->nullable()->comment('จำนวนผู้เข้าชม');
            $table->integer('member_id')->unsigned()->comment('รหัสสมาชิก');
            $table->integer('generation_id')->unsigned()->comment('รหัสรุ่น');
            $table->integer('color_id')->unsigned()->comment('รหัสสี');
            $table->integer('machine_id')->unsigned()->comment('รหัสขนาดเครื่องยนต์');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}

