<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members',function ($table){
            $table->foreign('province_id')->references('province_id')->on('provinces');
        });
        Schema::table('generations',function ($table){
            $table->foreign('brand_id')->references('brand_id')->on('brands');
        });
        Schema::table('pictures',function ($table){
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members',function ($table){
            $table->dropForeign('members_province_id_foreign');
        });
        Schema::table('generations',function ($table){
            $table->dropForeign('generations_brand_id_foreign');
        });
        Schema::table('pictures',function ($table){
            $table->dropForeign('pictures_product_id_foreign');
        });
    }
}
