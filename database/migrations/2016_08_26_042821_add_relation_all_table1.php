<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationAllTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts',function ($table){
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('member_id')->references('member_id')->on('members');
        });
        Schema::table('products',function ($table){
            $table->foreign('member_id')->references('member_id')->on('members');
            $table->foreign('generation_id')->references('generation_id')->on('generations');
            $table->foreign('color_id')->references('color_id')->on('colors');
            $table->foreign('machine_id')->references('machine_id')->on('machines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts',function ($table){
            $table->dropForeign('contacts_product_id_foreign');
            $table->dropForeign('contacts_member_id_foreign');
        });
        Schema::table('products',function ($table){
            $table->dropForeign('products_member_id_foreign');
            $table->dropForeign('products_generation_id_foreign');
            $table->dropForeign('products_color_id_foreign');
            $table->dropForeign('products_machine_id_foreign');
        });
    }
}
