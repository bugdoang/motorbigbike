<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts',function (Blueprint $table){
            $table->increments('contact_id');
            $table->string('contact_name',180)->nullable()->comment('ชื่อการติดต่อสอบถาม');
            $table->string('contact_detail',500)->nullable()->comment('รายละเอียดการติดต่อสอบถาม');
            $table->integer('product_id')->unsigned()->comment('รหัสสินค้า');
            $table->integer('member_id')->unsigned()->comment('รหัสสมาชิก');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
