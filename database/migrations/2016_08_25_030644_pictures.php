<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pictures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures',function (Blueprint $table){
            $table->increments('picture_id');
            $table->string('picture_name',180)->nullable()->comment('ชื่อรูปภาพ');
            $table->string('original_name',180)->nullable()->comment('ชื่อรูปภาพ');
            $table->string('picture_path',180)->nullable()->comment('เส้นทางเก็บข้อมูลรูปภาพ');
            $table->integer('product_id')->unsigned()->comment('รหัสสินค้า');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pictures');
    }
}
