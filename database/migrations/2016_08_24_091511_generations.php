<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Generations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generations',function (Blueprint $table){
            $table->increments('generation_id');
            $table->string('generation_name',180)->nullable()->comment('ชื่อรุ่น');
            $table->integer('brand_id')->unsigned()->comment('รหัสยี่ห้อ');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('generations');
    }
}
