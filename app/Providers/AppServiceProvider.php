<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $a = 1;
        $modules   = [];
        $modules[] = '\App\Modules\Home\ModuleServiceProvider';
        $modules[] = '\App\Modules\Product\ModuleServiceProvider';
        $modules[] = '\App\Modules\Register\ModuleServiceProvider';
        $modules[] = '\App\Modules\Login\ModuleServiceProvider';
        $modules[] = '\App\Modules\Profile\ModuleServiceProvider';



        foreach ($modules as $module){
            $this->app->register($module);
        }
    }
}
