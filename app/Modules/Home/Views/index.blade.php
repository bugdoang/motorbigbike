@extends('masterlayout')
@section('title',':: ศูนย์รวมมอเตอร์ไซค์บิ๊กไบค์มือสอง ::')

@section('content')
<!-- Page Content -->
    <div class="row">
        <div class="col-lg-12">
            <img style="margin: 10px auto;display: block" width="728" border="0" height="90" class="img_ad" alt="" src="https://tpc.googlesyndication.com/simgad/7040307393349581478">
        </div>
    </div>
    <div class="row">
        <!--start left-->
        <div class="col-lg-8 col-md-8">
            <div class="row">
                <h2 class="header-text myfont">รายการสินค้าใหม่ล่าสุด</h2>
                @for($i=0;$i<3;$i++)
                    <div class="item-border">
                        <a class="item-image pull-left" href="home.blade.php">
                            <img src="/assets/img/cbr650f.png">
                        </a>
                        <div class="item-product pull-left">
                            <h2 class="item-name myfont">Honda - CBR650F</h2>
                            <p class="item-detail">
                                สุดยอดบิ๊กไบค์สปอร์ตฟูลแฟริ่ง ออกแบบสวยงามตามสไตล์ CBR </br>
                                ที่เน้นรูปร่างดุดันและมีเอกลักษณ์ ด้วยเครื่องยนต์ 649 cc.<br/>
                                4 สูบ ระบายความร้อนด้วยน้ำ มั่นใจด้วยระบบเบรค ABS<br/>
                            </p>
                            <p class="item-price">
                                ราคา 300,000 บาท<br/>
                            </p>
                        </div>
                        <a class="booking pull-right myfont" href="#">
                            <i class="fa fa-comments" aria-hidden="true"></i><br/>Contact
                        </a>
                        <div class="owner myfont">
                            <a class="member-image">
                                <img src="/assets/img/tiffany.png" class="img-circle pull-left" alt="Cinque Terre" width="304" height="236">
                            </a>
                            <p class="member-name pull-left">
                                <span><i class="fa fa-user" aria-hidden="true"></i> Tiffany SNSD ทิฟฟานี่</span>
                                <span>ประกาศเมื่อ 31 สิงหาคม 2559 เวลา 15.30 น.</span>
                            </p>
                            <p class="member-num pull-right">
                                <span>จำนวนผู้เข้าชม 10,000 คน</span>
                            </p>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
        <!--start right-->
        <div class="col-lg-4 col-md-4">
            <h2 class="header-text myfont">รายการค้นหา</h2>
            <div class="item-search myfont">
                <form onsubmit="return SearchCar('Search.php?');" action="" method="post" name="search">
                    <label class="select-search">
                        <strong>ยี่ห้อ :</strong>
                    </label>
                    <div class="select_box_1">
                        <select id="s_type" class="custom-select" name="brand">
                            <option value="0">ทุกยี่ห้อ</option>
                            <option value="1">Aprillia</option>
                            <option value="2">Benelli</option>
                            <option value="3">Beta</option>
                            <option value="4">Bimota</option>
                            <option value="5">BMW</option>
                            <option value="6">Can-Am</option>
                            <option value="7">CCW</option>
                            <option value="8">Ducati</option>
                            <option value="9">EBR</option>
                            <option value="10">Harley-Davidson</option>
                            <option value="11">Kawasaki</option>
                            <option value="12">KTM</option>
                            <option value="13">Kymco</option>
                            <option value="14">Lotus</option>
                            <option value="15">MV Agusta</option>
                            <option value="16">RYUKA</option>
                            <option value="17">Stallion</option>
                            <option value="18">Suzuki</option>
                            <option value="19">Triumph</option>
                            <option value="20">Vespa</option>
                            <option value="21">Victory</option>
                            <option value="22">Voxan</option>
                            <option value="23">Yamaha</option>
                        </select>
                    </div>
                    <label class="select-search">
                        <strong>รุ่น :</strong>
                    </label>
                    <div class="select_box_1">
                        <select id="s_type" class="custom-select" name="brand">
                            <option value="0">ทุกรุ่น</option>
                            <option value="1">Aprillia</option>
                            <option value="2">Benelli</option>
                            <option value="3">Beta</option>
                            <option value="4">Bimota</option>
                            <option value="5">BMW</option>
                        </select>
                    </div>
                    <label class="select-search">
                        <strong>ตามปีรถ :</strong>
                    </label>
                    <div class="select_box_1">
                        <select id="s_type" class="custom-select" name="brand">
                            <option value="0">ทุกปี</option>
                            <option value="1">> 2015 ขึ้นไป</option>
                            <option value="2">2012 - 2014</option>
                            <option value="3">2009 - 2011</option>
                            <option value="4">ต่ำกว่าปี 2009</option>
                        </select>
                    </div>
                    <label class="select-search">
                        <strong>ช่วงราคา :</strong>
                    </label>
                    <div class="select_box_1">
                        <select id="s_type" class="custom-select" name="brand">
                            <option value="0">ทุกช่วงราคา</option>
                            <option value="1">< 150,000 บาท</option>
                            <option value="2">150,000 - 200,000 บาท</option>
                            <option value="3">> 200,000 บาท</option>
                        </select>
                    </div>
                    <a class="search-button pull-left myfont" href="#">
                        <i class="fa fa-search" aria-hidden="true"></i>  SEARCH
                    </a>
                </form>
            </div>
        </div>
    </div>
@stop
