@extends('masterlayout')
@section('title','Motorbigbike login')

@section('content')
<!-- Page Content -->
    <!--start left-->
    <div class="row">
        <div class="container-login">

            <div class="row" style="margin-top:20px">
                <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                    <form role="form">
                        <fieldset>
                            <h2 class="login-head myfont"><i class="fa fa-expeditedssl" aria-hidden="true"></i> เข้าสู่ระบบ</h2>
                            <hr class="colorgraph">
                            <div id="SignIn" class="thm-box myfont">
                                <a id="body_body_content_lnkBtnFacebookLogin" class="social-btn thm-btn" href="javascript:__doPostBack('ctl00$ctl00$ctl00$body$body$content$lnkBtnFacebookLogin','')">
                                    <i class="fa fa-facebook" aria-hidden="true"></i> เข้าสู่ระบบด้วย Facebook
                                </a>
                            </div>
                            <h2 class="register-text myfont">หรือ</h2>
                            <hr/>
                            <div class="form-group myfont">
                                <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address">
                            </div>
                            <div class="form-group myfont">
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password">
                            </div>
                            <span class="button-checkbox">
                                <button type="button" class="btn-text myfont" data-color="info">Remember Me</button>
                                <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                                <a href="" class="btn-text btn-link pull-right myfont">Forgot Password?</a>
                            </span>
                            <hr class="colorgraph">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="submit" class="btn btn-lg btn-success btn-block myfont" value="Sign In">
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <a href="" class="btn btn-lg btn-primary btn-block myfont">Register</a>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop
