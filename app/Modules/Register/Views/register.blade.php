@extends('masterlayout')
@section('title','Motorbigbike register')

@section('content')
<!-- Page Content -->
    <!--start left-->
    <div class="row">
                <div class="container-register">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                            <form role="form">
                                <h2 class="register-head myfont"><i class="fa fa-user-plus" aria-hidden="true"></i>
                                    สมัครสมาชิก Motorbigbike ฟรี !</h2>
                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group myfont">
                                            <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group myfont">
                                            <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group myfont">
                                    <input type="text" name="display_name" id="display_name" class="form-control input-lg" placeholder="Display Name" tabindex="3">
                                </div>
                                <div class="form-group myfont">
                                    <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="4">
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group myfont">
                                            <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group myfont">
                                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="6">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-sm-3 col-md-3">
                                        <span class="agree-checkbox myfont">
                                            <input type="checkbox" name="t_and_c" id="t_and_c"  value="1"> I Agree
                                        </span>
                                    </div>
                                    <div class="col-xs-8 col-sm-9 col-md-9">
                                        <h7 class="agree-checkbox myfont">กด "Register" เพื่อยอมรับ <a href="#" data-toggle="modal" data-target="#t_and_c_m">เงื่อนไขการใช้บริการ </a></h7>
                                    </div>
                                </div>
                                <hr/>
                                <h2 class="register-text myfont">หรือ</h2>
                                <div id="SignIn" class="thm-box myfont">
                                    <a id="body_body_content_lnkBtnFacebookLogin" class="social-btn thm-btn" href="javascript:__doPostBack('ctl00$ctl00$ctl00$body$body$content$lnkBtnFacebookLogin','')">
                                        <i class="fa fa-facebook" aria-hidden="true"></i> สมัครด้วย Facebook
                                    </a>
                                </div>
                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6"><input type="submit" value="Register" class="btn btn-primary btn-block btn-lg myfont" tabindex="7"></div>
                                    <div class="col-xs-12 col-md-6"><a href="#" class="btn btn-success btn-block btn-lg myfont">Sign In</a></div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <!-- Modal -->
                    <div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                                </div>


                                <div class="modal-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
                                </div>


                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                </div>
    </div>
@stop
