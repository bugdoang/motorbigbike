<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/4/2016
 * Time: 5:08 PM
 */

namespace App\Modules\Product\Controllers;


use App\Http\Controllers\Controller;

class Product extends Controller
{
    public function index()
    {
        return view('product::product');
    }
}