@extends('masterlayout')
@section('title','Motorbigbike productdetail')

@section('content')
    <div class="row">
        <div class="container-fluid">
            <div class="content-wrapper">
                <div class="item-container">
                    <h2 class="product-name myfont">HONDA - CBR650F NEW 2016 !</h2>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-8 col-md-8">
                            <img class="image-detail" src="/assets/img/tiffany7.jpg">
                            <div class="row">
                                <div class="image-container">
                                    <div class="col-lg-3 col-md-3">
                                        <a id="item-1" class="service1-item">
                                            <img class="image-detail1" src="/assets/img/cbr650f1.jpg">
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <a id="item-2" class="service1-item">
                                            <img class="image-detail1" src="/assets/img/cbr650f2.jpg">
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <a id="item-2" class="service1-item">
                                            <img class="image-detail1" src="/assets/img/cbr650f3.jpg">
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <a id="item-2" class="service1-item">
                                            <img class="image-detail1" src="/assets/img/tiffany3.jpg">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="item-border">
                                <div class="item-product pull-left">
                                    <h2 class="item-name myfont">Honda - CBR650F</h2>
                                    <p class="item-detail">
                                        สุดยอดบิ๊กไบค์สปอร์ตฟูลแฟริ่ง ออกแบบสวยงามตามสไตล์ CBR </br>
                                        ที่เน้นรูปร่างดุดันและมีเอกลักษณ์ ด้วยเครื่องยนต์ 649 cc.<br/>
                                        4 สูบ ระบายความร้อนด้วยน้ำ มั่นใจด้วยระบบเบรค ABS<br/>
                                    </p>
                                    <p class="item-price">
                                        ราคา 300,000 บาท<br/>
                                    </p>
                                    <p class="item-detail">
                                        Honda cbr650f ปี 2015 สภาพของแต่งล้วนๆ ไม่มีล้ม ไม่มีชน ไม่แปะ ไมล์น้อย 10,000 km กุญแจ2ดอก เซอวิสบุ้คมี คู่มือครบ เล่มพร้อมโอน พรบไม่ขาดต่อ
                                        ของแต่งมีดังนี้<br/>
                                        - ท่อ Akrapovic<br/>
                                        - ท้ายสั้น motoplay<br/>
                                        - ทะเบียนยัดซุ้ม<br/>
                                        - คาร์บอนกันรอยถัง<br/>
                                        - ก้านเบรค bikers<br/>
                                        - ก้านครัซ bikers<br/>
                                        รถสภาพดี เครื่องแน่นๆๆแน่นอน<br/>
                                        ราคา 255,000 หรือจะ 250,000 (ท่อเดิม) ซื้อสดลดได้ครับ<br/>
                                        หรือดาวน์ 58,000 หรือจะ 53,000 (ท่อเดิม) (ราคารวมค่าจัดไฟแนนซ์แล้วครับ)<br/>
                                        ผ่อน 5,800 * 48 เดือน<br/>
                                        หรือจะผ่อน 5,000 * 60 เดือน<br/>
                                        สอบถามรายละเอียด ติดต่อนัดดูรถ<br/>
                                        Line:hidespalados<br/>
                                        Tel:0909814157<br/>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <h2 class="header-text margin-top0 myfont"><i class="fa fa-user" aria-hidden="true"></i> ผู้ลงประกาศ</h2>
                            <div class="owner1 myfont">
                                <a class="member-image">
                                    <img src="/assets/img/tiffany.png" class="img-circle pull-left" alt="Cinque Terre" width="304" height="236">
                                </a>
                                <p class="member-name1">
                                    <span><i class="fa fa-user" aria-hidden="true"></i> Tiffany SNSD ทิฟฟานี่</span>
                                </p>
                                <hr/>
                                <p class="member-post">
                                    <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> คุยกับผู้ขาย</a>
                                </p>
                                <hr/>
                                <p class="member-post">
                                    <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> กดดูเบอร์โทรศัพท์ผู้ขาย</a>
                                </p>
                                <hr/>
                                <p class="member-post">
                                    <span><i class="fa fa-tag" aria-hidden="true"></i> 300,000 บาท</span>
                                </p>
                                <hr/>
                                <p class="member-post">
                                    <span><i class="fa fa-eye" aria-hidden="true"></i> จำนวนผู้เข้าชม 10,000 คน</span>
                                </p>
                                <hr/>
                                <p class="member-post1">
                                    <span><i class="fa fa-info" aria-hidden="true"></i> ประกาศเมื่อ 31 สิงหาคม 2559 เวลา 15.30 น.</span>
                                </p>
                            </div>
                            <h2 class="header-text myfont"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ประกาศแนะนำ</h2>
                            @for($i=0;$i<3;$i++)
                                <div class="item-border margin-top10">
                                    <a class="item-image display-block" href="home.blade.php">
                                        <img src="/assets/img/cbr650f.png">
                                    </a>
                                    <div class="item-product">
                                        <h2 class="item-name myfont">Honda - CBR650F</h2>
                                        <p class="item-detail font-size">
                                            สุดยอดบิ๊กไบค์สปอร์ตฟูลแฟริ่งออกแบบสวยงามตามสไตล์ CBR
                                            ที่เน้นรูปร่างดุดันและมีเอกลักษณ์ด้วยเครื่องยนต์ 649 cc.<br/>
                                            4 สูบ ระบายความร้อนด้วยน้ำ มั่นใจด้วยระบบเบรค ABS<br/>
                                        </p>
                                        <p class="item-price">
                                            ราคา 300,000 บาท<br/>
                                        </p>
                                    </div>
                                    <a class="booking margin myfont" href="#">
                                        <i class="fa fa-comments" aria-hidden="true"></i> Contact
                                    </a>

                                    <div class="owner margin-top5 myfont">
                                        <a class="member-image">
                                            <img src="/assets/img/tiffany.png" class="img-circle pull-left" alt="Cinque Terre" width="304" height="236">
                                        </a>
                                        <p class="member-name">
                                            <span><i class="fa fa-user" aria-hidden="true"></i> Tiffany SNSD ทิฟฟานี่</span>
                                            <span>ประกาศเมื่อ 31 สิงหาคม 2559 เวลา 15.30 น.</span>
                                        </p>
                                    </div>
                                </div>
                            @endfor
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop