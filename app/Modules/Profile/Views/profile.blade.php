@extends('masterlayout')
@section('title','Motorbigbike profile')

@section('content')
<!-- Page Content -->
<div class="row">
    <div class="col-lg-12">
        <img style="margin: 10px auto;display: block" width="728" border="0" height="90" class="img_ad" alt="" src="https://tpc.googlesyndication.com/simgad/7040307393349581478">
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-1 col-md-1">

        </div>
        <div class="col-lg-10 col-md-10">
            <h2 class="header-text myfont"><i class="fa fa-user" aria-hidden="true"></i> ข้อมูลส่วนตัว</h2>
            <div class="panel panel-info">
                <ul id="myTab" class="nav nav-tabs nav_tabs">
                    <li class="active"><a href="#service-one" data-toggle="tab">ทั่วไป</a></li>
                    <li><a href="#service-two" data-toggle="tab"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> เปลี่ยนรหัสผ่าน</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="service-one">
                        <section class="container1 product-info">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="/assets/img/tiffany.png" class="img-circle img-responsive"> </div>
                                    <div class=" col-md-9 col-lg-9 ">
                                        <table class="table table-user-information">
                                            <tbody>
                                                <tr>
                                                    <td>ชื่อ :</td>
                                                    <td>นามสกุล :</td>
                                                </tr>
                                                <tr>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                </tr>
                                                <tr>
                                                    <td>หมายเลขบัตรประจำตัวประชาชน / หมายเลขหนังสือเดินทาง</td>
                                                </tr>
                                                <tr>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                </tr>
                                                <tr>
                                                    <td>อีเมล์ :</td>
                                                    <td>อีเมล์สำรอง :</td>
                                                </tr>
                                                <tr>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                </tr>
                                                <tr>
                                                    <td>เบอร์โทรศัพท์ :</td>
                                                </tr>
                                                <tr>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                </tr>
                                                <tr>
                                                    <td>ชื่อผู้ใช้ :</td>
                                                    <td>รหัสผ่าน :</td>
                                                </tr>
                                                <tr>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                </tr>
                                                <tr>
                                                    <td>ประเภทสมาชิก :</td>
                                                </tr>
                                                <tr>
                                                    <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane fade" id="service-two">
                        <section class="container1">
                            <div class="col-md-8 col-lg-8">

                            </div>
                            <div class=" col-md-4 col-lg-4">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>รหัสผ่านปัจจุบัน <span class="font-color-red"> * </span> :</td>
                                    </tr>
                                    <tr>
                                        <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                    </tr>
                                    <tr>
                                        <td>รหัสผ่านใหม่ <span class="font-color-red"> * </span> :</td>
                                    </tr>
                                    <tr>
                                        <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                    </tr>
                                    <tr>
                                        <td>ยืนยันรหัสผ่านใหม่ <span class="font-color-red"> * </span> :</td>
                                    </tr>
                                    <tr>
                                        <td><input id="" class="input-block-level font-small" type="text" name=""></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-md-1">

        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-11 col-md-11">
        <div class="pull-right">
            <a href="" class="btn background-colorred btn-lg btn-primary btn-block myfont">บันทึก</a>
        </div>
    </div>
    <div class="col-lg-1 col-md-1">

    </div>
</div>
@stop

