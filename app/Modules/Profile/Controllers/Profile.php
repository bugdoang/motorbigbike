<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/4/2016
 * Time: 5:08 PM
 */

namespace App\Modules\Profile\Controllers;


use App\Http\Controllers\Controller;

class Profile extends Controller
{
    public function index()
    {
        return view('profile::profile');
    }
}